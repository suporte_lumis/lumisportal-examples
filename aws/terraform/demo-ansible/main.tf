terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "us-east-1"
  version = "~> 2.0"
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.default.id
  name   = "default"
}

resource "aws_db_parameter_group" "default" {
  name   = "ansible-mysql"
  family = "mysql5.7"

  parameter {
    name  = "log_bin_trust_function_creators"
    value = "1"
  }

  parameter {
    name  = "max_allowed_packet"
    value = "134217728"
  }

  
}

module "db" {
    source  = "terraform-aws-modules/rds/aws"
    version = "~> 2.0"

    identifier = "demodb"

    vpc_security_group_ids = ["sg-05453cef8b3ee3e6e"]

    engine            = "mysql"
    engine_version    = "5.7.19"
    instance_class    = "db.t2.small"
    allocated_storage = 5

    name              = "lumisportal"
    username          = "lumisportal"
    password          = "YourPwdShouldBeLongAndSecure!"
    port              = "3306"

    multi_az                  = false
    backup_retention_period   = 0
    subnet_ids                = data.aws_subnet_ids.all.ids
    family                    = "mysql5.7"

    major_engine_version =   "5.7"
    deletion_protection   = false

    parameter_group_name = aws_db_parameter_group.default.id

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"
}





module "ec2" {
   source                 = "terraform-aws-modules/ec2-instance/aws"

  instance_count = 2

  name          = "lumisportal"
  ami           = "ami-0b69ea66ff7391e80"
  instance_type = "t2.medium"
  subnet_id     = tolist(data.aws_subnet_ids.all.ids)[0]

  key_name = "ansible-poc"

  
  vpc_security_group_ids = ["sg-05453cef8b3ee3e6e"]
  
  associate_public_ip_address = true

  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 10
    },
  ]

  ebs_block_device = [
    {
      device_name = "/dev/sdf"
      volume_type = "gp2"
      volume_size = 5
     
    }
  ]
}
