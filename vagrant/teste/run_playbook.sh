export COLUMNS=80
ANSIBLE_CONFIG=./ansible_default.cfg ansible-playbook -i inventories/inventory --limit mysql  configure.yml > ./logs/default &

ANSIBLE_CONFIG=./ansible_pipelining.cfg ansible-playbook -i inventories/inventory --limit lumis1  configure.yml > ./logs/default &

ANSIBLE_CONFIG=./ansible_mitogen.cfg ansible-playbook -i inventories/inventory --limit lumis2  configure.yml > ./logs/default &