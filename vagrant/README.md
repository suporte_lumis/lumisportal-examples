
Fazendo download das dependências

    ansible-galaxy install -r requirements.yml

Iniciando a instancia do vagrant
    
    vagrant up 
    vagrant ssh-config
#### ansible-playbook


**--ask-become-pass**

parâmetro que força o analista digitar a senha do usuário que o ansible vai usar.

**--ask-vault-pass**

Parêmetro que solicita a senha para descriptografar.

#### ansible-vault

Ansible Vault is a feature of ansible that allows you to keep sensitive data such as passwords or keys in encrypted files, rather than as plaintext in playbooks or roles. These vault files can then be distributed or placed in source control.

To enable this feature, a command line tool - ansible-vault - is used to edit files, and a command line flag (--ask-vault-pass, --vault-password-file or --vault-id) is used. Alternately, you may specify the location of a password file or command Ansible to always prompt for the password in your ansible.cfg file. These options require no command line flag usage.

##### encrypt

    ansible-vault encrypt secrets.yml

##### decrypt

    ansible-vault decrypt secrets.yml

